

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	//Const count how many ships for this level
	const int COUNT = 21;

	//array of horizontal offsets for each individual ship
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	//array of additional delay bewteen each ship
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	//delay before enemy ships first appear, originally 3
	float delay = 1.0; // start delay
	Vector2 position;

	//For loop creating each ship
	for (int i = 0; i < COUNT; i++)
	{
		//add the delay from the delays array position
		delay += delays[i];
		//define the start postition for each ship
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		//create a pointer on the heap for each enemy ship
		BioEnemyShip *pEnemy = new BioEnemyShip();
		//define charecteristics of the enemy ship, texture, level, position and delay from start
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		//add object enemy ship
		AddGameObject(pEnemy);
	}

	//pass the input pResourceManager to LoadContent
	Level::LoadContent(pResourceManager);
}

