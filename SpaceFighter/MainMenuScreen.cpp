
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	//Get the logo and center it
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items

	//2 Items in the menu
	const int COUNT = 2;
	//declare pointer pItem
	MenuItem *pItem;
	//Set the font size originally was 20
	Font::SetLoadSize(30, true);
	//Set the font type
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	//Set the display to show items
	SetDisplayCount(COUNT);

	//Declare enum Items start game and quit
	enum Items { START_GAME, QUIT };
	//Declare an array of strings for menu items
	std::string text[COUNT] = { "Start Game", "Quit" };

	//Loop through each menu item
	for (int i = 0; i < COUNT; i++)
	{
		//declare each menu item with the desired text
		pItem = new MenuItem(text[i]);
		//set the position constant for x.  Y has an initial offset and an additonal offset for each menu item
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		//set the items font
		pItem->SetFont(pFont);
		//set the items color
		pItem->SetColor(Color::DimGray);
		//default select the first item in the list
		pItem->SetSelected(i == 0);
		//add item to the screen
		AddMenuItem(pItem);
	}

	//provide menu items so functions can be called when selected
	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);
}

void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Green);
		else pItem->SetColor(Color::DimGray);
	}

	MenuScreen::Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
